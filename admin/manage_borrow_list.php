<?php
include ('head.php');
?>
<div class="layui-body">
    <!-- 用户管理-用户信息列表 -->
    <div style="padding:15px;">
        <h2>申请借阅</h2>
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">申请列表</a></li>
                <li><a href="manage_return_list.php">归还书籍</a></li>
            </ul>
        </div>
        <!--显示表的内容-->
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">

                <script type="text/html" id="toolbar">
                    <div class="layui-btn-container">
                        <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="agree">同意</a>
                        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="reject">拒绝</a>
                    </div>
                </script>
                <table class="layui-table" lay-data="{
                        height:550,
                        page:true,
                        id:'id_table',
                        toolbar:flase
                    }"
                       lay-filter="test">
                    <thead>
                    <tr>
                        <td lay-data="{field:'id',sort:true}">借阅人姓名</td>
                        <td lay-data="{field:'title'}">图书名称</td>
                        <td lay-data="{field:'book_id'}">图书ID</td>
                        <td lay-data="{field:'',toolbar:'#toolbar',width:200}">操作</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sql="select * from borrowers";
                    $rs=mysqli_query($conn,$sql);
                    if($rs){
                        while ($row=mysqli_fetch_assoc($rs)){
                            echo '<tr>';
                            echo '<td>'.$row['real_name'].'</td>';
                            echo '<td>'.$row['book_title'].'</td>';
                            echo '<td>'.$row['book_id'].'</td>';
                            echo '<td></td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <script>
                    layui.use('table',function (){
                        var table=layui.table;
                        table.on('tool(test)',function (obj){
                            var tr=obj.data;
                            let arr=Object.values(tr);
                            var eventName= obj.event;
                            if(eventName=='agree')
                            {
                                //同意
                                layer.confirm("您确认同意吗？",function (index){
                                    obj.del();
                                    layer.close(index);
                                    window.location.href="borrowers_agree.php?book_id="+arr[2];
                                })
                            }else if(eventName=='reject'){
                                //拒绝
                                layer.confirm("您确认拒绝吗？",function (index){
                                    obj.del();
                                    layer.close(index);
                                    window.location.href="borrowers_reject.php?book_id="+arr[2];
                                })

                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>

</div>

<?php
include ('foot.php');

