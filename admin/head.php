<?php
//头文件声明
header('Content-Type:text/html; charset=utf-8');
//连接数据库
$servername="localhost";
$username="root";
$password="123456";
$dbname="lmsdb";//创建链接
//检测连接
$conn=mysqli_connect($servername,$username,$password,$dbname);
if(!$conn){
    die("connection failed:".mysqli_connect_error());
}
//定义弹窗方法
function alert($str,$url){
    echo '<script> alert("'.$str.'");location.href="'.$url.'";</script>';
}

//接受由登陆界面传来的数据
session_start();

//是否登录有效，如数据库中的用户名与当前获取的用户名不一致
//则无权访问
if(!isset($_SESSION['username'])){
    alert('登录失效，请重新登录','../login.php');
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>图书管理系统首页</title>
    <link rel="stylesheet" href="../layui/css/layui.css">
    <script src="../layui/layui.js"></script>
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo layui-hide-xs layui-bg-black"><big><b>图书管理系统</b></big></div>
        <!-- 头部区域-->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item layui-hide-xs"><a href="index.php"><big><b>首页</b></big></a></li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item layui-hide-xs"><a href="../gpt.php"><big><b>人工咨询</b></big></a></li>
            <li class="layui-nav-item layui-hide-xs"><a href="../logout.php"><b>退出登陆</b></a></li>
            <li class="layui-nav-item layui-hide layui-show-md-inline-block">
                <a href="javascript:;">
                    <b>个人中心</b>
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="mine_info_list.php">个人信息</a></dd>
                    <dd><a href="mine_borrow_new.php">借阅申请</a></dd>
                    <dd><a href="mine_borrow_list.php">申请列表</a></dd>
                    <dd><a href="mine_book_list.php">我的书籍</a></dd>
                </dl>
            </li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">图书管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="../book_manage/add_book.php">新增图书</a></dd>
                        <dd><a href="../book_manage/book_list.php">图书列表</a></dd>
                        <dd><a href="javascript:;">借阅管理</a></dd>
                        <dd><a href="javascript:;">损坏管理</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">分类管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="assort_list.php">分类列表</a></dd>
                        <dd><a href="assort_new.php">新增分类</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">位置管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="position_list.php">位置列表</a></dd>
                        <dd><a href="position_new.php">新增位置</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">借阅管理<?php echo $span ?></a>
                    <dl class="layui-nav-child">
                        <dd><a href="manage_borrow_list.php">申请列表</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">用户管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="../admin/user_list.php">用户列表</a></dd>
                        <dd><a href="../admin/user_new.php">新增用户</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">权限设置</a>
                    <dl class="layui-nav-child">
                        <dd><a href="limits_list.php">修改权限</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>