<?php
include ('head.php');
?>
    <div class="layui-body">
        <!-- 用户管理-用户信息列表 -->
        <div style="padding:15px;">
            <h2>归还书籍</h2>
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <li><a href="manage_borrow_list.php">申请列表</a></li>
                    <li class="layui-this">归还书籍</a></li>
                </ul>
            </div>
            <!--显示表的内容-->
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">

                    <script type="text/html" id="toolbar">
                        <div class="layui-btn-container">
                            <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="return">归还</a>

                        </div>
                    </script>
                    <table class="layui-table" lay-data="{
                        height:550,
                        page:true,
                        id:'id_table',
                        toolbar:flase
                    }"
                           lay-filter="test">
                        <thead>
                        <tr>
                            <td lay-data="{field:'id',sort:true}">待归还图书ID</td>
                            <td lay-data="{field:'title'}">待归还图书名称</td>
                            <td lay-data="{field:'',toolbar:'#toolbar',width:200}">操作</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT * FROM books";
                        $rs = mysqli_query($conn, $sql);
                        if ($rs) {
                            while ($row = mysqli_fetch_assoc($rs)) {
                                // 判断 available 值
                                if ($row['available'] == 1) {
                                    continue; // 如果 available 值为 1，则跳过当前行的输出
                                }

                                echo '<tr>';
                                echo '<td>' . $row['id'] . '</td>';
                                echo '<td>' . $row['title'] . '</td>';
                                echo '<td></td>';
                                echo '</tr>';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <script>
                        layui.use('table',function (){
                            var table=layui.table;
                            table.on('tool(test)',function (obj){
                                var tr=obj.data;
                                let arr=Object.values(tr);
                                var eventName= obj.event;
                                if(eventName=='return')
                                {
                                    //同意
                                    layer.confirm("您确认归还吗？",function (index){
                                        obj.del();
                                        layer.close(index);
                                        window.location.href="manage_borrowers_return.php?id="+arr[0];
                                    })
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>

    </div>

<?php
include ('foot.php');

