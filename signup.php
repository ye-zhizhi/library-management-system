<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>图书馆管理系统注册界面</title>
    <link rel = "stylesheet" href="layui/css/layui.css">
    <script src="layui/layui.js"></script>
    <style>
        body
        {
            background-image: url("./picture/log1.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
<div class="layui-container">
    <div style="height: 150px"></div>
    <div class="layui-row">
    <!--里面的内容-->
        <div class="layui-col-md4" style="height: 150px"></div>
        <div class="layui-col-md4" style="height: 150px">
            <p style="text-align: center;font-size:25px">
                图书管理系统注册界面
            </p>
            <!-- 登录框-->
            <form class="layui-form" action="signup_check.php" method="post">
                <div style="height: 20px"></div>
                <input type="text" name="username" placeholder="请输入用户名" class="layui-input">
                <div style="height: 15px"></div>
                <input type="password" name="password1" placeholder="请输入密码" class="layui-input">
                <div style="height: 15px"></div>
                <input type="password" name="password2" placeholder="请输入再次密码" class="layui-input">
                <div style="height: 15px"></div>
                <input type="submit" value="注册" class="layui-btn layui-btn-normal layui-btn-fluid">
            </form>
        </div>
        <div class="layui-col-md4" style="height: 150px"></div>
    </div>
</div>
<script>
    layui.use('form',function(){
        var form=layui.form;
    });
</script>
</body>
</html>